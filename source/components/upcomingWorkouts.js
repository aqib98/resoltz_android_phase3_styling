/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput

} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';

const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');

import SplashScreen from 'react-native-smart-splash-screen'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import  Entypo from 'react-native-vector-icons/Entypo'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import Feather from 'react-native-vector-icons/Feather'
import Ionicons from 'react-native-vector-icons/Ionicons'

import { TabNavigator } from 'react-navigation';


import styles from './styles.js'



export default class UpcomingWorkouts extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
      width:0,
      height : 0
    };
  }
  componentDidMount () {


  }



    render() {
      const {navigation} = this.props

      return (
        <View style={styles.upcomingWorkouts_main_body}>
                <View style={styles.upcomingWorkouts_bg_img_view}>
                  <Image style={styles.upcomingWorkouts_bg_img} source={{ uri: 'gradientbg' }}/>
                </View>
                <View style={styles.upcomingWorkouts_header}>
                <Text style={styles.upcomingWorkouts_heading}>
                    UPCOMING WORKOUTS
                </Text>
                </View>

                <ScrollView style={styles.upcomingWorkouts_scrollView}>

                        <View style={styles.upcomingWorkouts_content_view}>
                            <View style={styles.upcomingWorkouts_content}>


                                <Text  style={styles.tuesday_heading}>
                                  TUESSDAY,DECEMBER 13TH 1
                                </Text>
                                <TouchableOpacity>
                                <Image
                                  style={{height:185,
                                  width:'100%'}}
                                  source={{ uri: 'buttonimg' }}
                                />
                                </TouchableOpacity>
                                <Text  style={styles.tuesday_heading}>
                                  CORE CRUSHER ABS1
                                </Text>
                                <View style={styles.whiteline}>
                                </View>
                                <Text  style={styles.tuesday_heading}>
                                  TUESSDAY,DECEMBER 13TH 222
                                </Text>
                                <TouchableOpacity>
                                <Image
                                  style={{height:185,
                                  width:'100%'}}
                                  source={{ uri: 'buttonimg' }}
                                />
                                </TouchableOpacity>
                                <Text  style={styles.tuesday_heading}>
                                  CORE CRUSHER ABS2
                                </Text>
                                <View style={styles.whiteline}></View>

                                <Text  style={styles.tuesday_heading}>
                                  TUESSDAY,DECEMBER 13TH 3
                                </Text>
                                <TouchableOpacity>
                                <Image
                                  style={{height:185,
                                  width:'100%'}}
                                  source={{ uri: 'buttonimg' }}
                                />
                                </TouchableOpacity>
                                <Text  style={styles.tuesday_heading}>
                                  CORE CRUSHER ABS3
                                </Text>
                                <View style={styles.whiteline}></View>

                                <Text  style={styles.tuesday_heading}>
                                  TUESSDAY,DECEMBER 13TH 4
                                </Text>
                                <TouchableOpacity>
                                <Image
                                  style={{height:185,
                                  width:'100%'}}
                                  source={{ uri: 'buttonimg' }}
                                />
                                </TouchableOpacity>
                                <Text  style={styles.tuesday_heading}>
                                  CORE CRUSHER ABS4
                                </Text>
                                <View style={styles.whiteline}></View>

                                <Text  style={styles.tuesday_heading}>
                                  TUESSDAY,DECEMBER 13TH 5
                                </Text>
                                <TouchableOpacity>
                                <Image
                                  style={{height:185,
                                  width:'100%'}}
                                  source={{ uri: 'buttonimg' }}
                                />
                                </TouchableOpacity>
                                <Text  style={styles.tuesday_heading}>
                                  CORE CRUSHER ABS5
                                </Text>
                                <View style={styles.whiteline}></View>

                                <Text  style={styles.tuesday_heading}>
                                  TUESSDAY,DECEMBER 13TH 6
                                </Text>
                                <TouchableOpacity>
                                <Image
                                  style={{height:185,
                                  width:'100%'}}
                                  source={{ uri: 'buttonimg' }}
                                />
                                </TouchableOpacity>
                                <Text  style={styles.tuesday_heading}>
                                  CORE CRUSHER ABS6
                                </Text>
                                <View style={styles.whiteline}></View>

                                <Text  style={styles.tuesday_heading}>
                                  TUESSDAY,DECEMBER 13TH 7
                                </Text>
                                <TouchableOpacity>
                                <Image
                                  style={{height:185,
                                  width:'100%'}}
                                  source={{ uri: 'buttonimg' }}
                                />
                                </TouchableOpacity>
                                <Text  style={styles.tuesday_heading}>
                                  CORE CRUSHER ABS7
                                </Text>
                                <View style={styles.whiteline}></View>

                                <Text  style={styles.tuesday_heading}>
                                  TUESSDAY,DECEMBER 13TH 8
                                </Text>
                                <TouchableOpacity>
                                <Image
                                  style={{height:185,
                                  width:'100%'}}
                                  source={{ uri: 'buttonimg' }}
                                />
                                </TouchableOpacity>
                                <Text  style={styles.tuesday_heading}>
                                  CORE CRUSHER ABS8
                                </Text>
                                <View style={styles.whiteline}></View>

                                <Text  style={styles.tuesday_heading}>
                                  TUESSDAY,DECEMBER 13TH 9
                                </Text>
                                <TouchableOpacity>
                                <Image
                                  style={{height:185,
                                  width:'100%'}}
                                  source={{ uri: 'buttonimg' }}
                                />
                                </TouchableOpacity>
                                <Text  style={styles.tuesday_heading}>
                                  CORE CRUSHER ABS8
                                </Text>
                                <View style={styles.whiteline}></View>

                                <Text  style={styles.tuesday_heading}>
                                  TUESSDAY,DECEMBER 13TH 10
                                </Text>
                                <TouchableOpacity>
                                <Image
                                  style={{height:185,
                                  width:'100%'}}
                                  source={{ uri: 'buttonimg' }}
                                />
                                </TouchableOpacity>
                                <Text  style={styles.tuesday_heading}>
                                  CORE CRUSHER ABS8
                                </Text>
                                <View style={styles.whiteline}></View>

                                <Text  style={styles.tuesday_heading}>
                                  TUESSDAY,DECEMBER 13TH 11
                                </Text>
                                <TouchableOpacity>
                                <Image
                                  style={{height:185,
                                  width:'100%'}}
                                  source={{ uri: 'buttonimg' }}
                                />
                                </TouchableOpacity>
                                <Text  style={styles.tuesday_heading}>
                                  CORE CRUSHER ABS8
                                </Text>
                                <View style={styles.whiteline}></View>


                            </View>


                        </View>

                </ScrollView>

                <View style={styles.wat_log_track_icons}>
                    <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 50, height: 50 }} source={{uri:'rgtarrow'}} /><Text style={{fontSize:10,color:'#fff'}}>Watch Video</Text></View>
                    <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 50, height: 50 }} source={{uri:'timer'}} /><Text style={{fontSize:10,color:'#fff'}}>Track activity</Text></View>
                    <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 50, height: 50 }} source={{uri:'fingerup'}} /><Text style={{fontSize:10,color:'#fff'}}>Log activity</Text></View>
                </View>

                <View style={styles.cross_icon}>
                  <Image style={{  width: 45, height: 45 }} source={{uri:'cross'}} />
                </View>
                <View style={styles.trans_bg}>
                </View>
                <View style={styles.btm_tabNavigation}>
                    <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 32, height: 28, top:3, position:'absolute' }} source={{uri:'feedicon'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}>FEED</Text></View>
                    <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 32, height: 32, top:1, position:'absolute'  }} source={{uri:'profileicon'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}>PROFILE</Text></View>
                    <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 45, height: 45, marginTop:0, marginBottom:4 }} source={{uri:'bottom_tab_middlelogo'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}> </Text></View>
                    <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 28, height: 31, top:2, position:'absolute'  }} source={{uri:'notificationicon'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}>NOTIFICATION</Text></View>
                    <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 32, height: 32, top:1, position:'absolute'  }} source={{uri:'btnmenuicon'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}>MORE</Text></View>
                </View>
              </View>



      );
    }
}
