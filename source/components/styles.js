
import {

  StyleSheet,


} from 'react-native';
const Dimensions = require('Dimensions');
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function wp (percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}

const slideHeight = viewportHeight * 0.4;
const slideWidth = wp(90);
const itemHorizontalMargin = wp(2);
const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;

const styles = StyleSheet.create({
  mainBody:{
    backgroundColor: '#2d2e37',
    marginTop : 0,
    padding:0,
    height:Dimensions.get('window').height,
    width:Dimensions.get('window').width,
    fontSize:20
  },
  header : {
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'row'

  },
  header_wrkout_details:{
  },
  body1 : {
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'column',
    position:'relative'
  } ,
  body1a : {
    alignItems:'center',
     paddingBottom:120

  } ,
  footer : {
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'row',
    position:'relative',
    bottom:0
  },
  signup_temp_form:{
   alignItems:'center'
  },
  textStyle : {
    color:'#fff',
    alignItems:'center',
    justifyContent:'center',
    fontFamily :'arial',
    fontSize:16,
    fontWeight:'bold',
      textAlign:'center', width:280,
      marginBottom:40


  },


  submitFB:{
   marginRight:40,
   marginLeft:40,
   marginTop:0,
   marginBottom:20,
   paddingTop:16,
   paddingBottom:16,
   backgroundColor:'#465897',
   borderRadius:30,
   position:'relative',
   borderColor: '#fff',
   width:242,

 },
 submitTW:{
  marginRight:40,
  marginLeft:40,
  marginTop:0,
  marginBottom:20,
  paddingTop:16,
  paddingBottom:16,
  backgroundColor:'#599ef3',
  borderRadius:30,
  position:'relative',
  borderColor: '#fff',
  width:242,

},
submitGM:{
 marginRight:40,
 marginLeft:40,
 marginTop:0,
 marginBottom:20,
 paddingTop:16,
 paddingBottom:16,
 backgroundColor:'#e15350',
 borderRadius:30,
 position:'relative',
 borderColor: '#fff',
 width:242,

},
submit:{
  height:60,
  marginBottom:20,
  width:Dimensions.get('window').width

},

 submitText:{
     color:'#fff',
     textAlign:'center',
 },
 buttonText:{
   color:'#fff',
   textAlign:'center',
   fontSize : 15,


 },
 footerText:{
   fontSize:20,
   color:'#fff',
   fontWeight:'bold',
   textAlign:'center',
   paddingTop:15
 },
 textInput_login:{
   marginRight:40,
   marginLeft:40,
   marginTop:0,
   marginBottom:20,
   paddingTop:11,
   paddingBottom:12,
   paddingLeft:25,
   backgroundColor:'#202124',
   borderRadius:30,
   position:'relative',
   color: '#fff',
   width:242,

 },
 textInput_signup:{
   marginRight:40,
   marginLeft:40,
   marginTop:0,
   marginBottom:20,
   paddingTop:11,
   paddingBottom:12,
   paddingLeft:25,
   backgroundColor:'#202124',
   borderRadius:30,
   position:'relative',
   color: '#fff',
   width:242,

 },
   forTxt:{paddingBottom:25,color:'#fff', paddingTop:25},

   nonStudent:{paddingBottom:25,color:'#fff', paddingTop:45},

   topSignupTxt:{color:'#F5F5FF',
   textAlign:'center',
   fontSize : 20,
   opacity:0.5,
   fontWeight:'bold',paddingTop:15},

   save_btnTxt : {
     fontSize:20,
     paddingTop:35,
     fontWeight:'bold',
     textAlign:'center',
     color:'#fff',
     justifyContent: 'center',
     alignItems: 'center'},

     save_btnImg:{
         resizeMode: 'contain',

         height:100,
          width:Dimensions.get('window').width,
          zIndex:0
     },
     wrkdetails_name1:{
       fontSize:18 ,color:'#fff',fontWeight:'bold', paddingLeft:24, paddingTop:20,position:'relative', height:150
     },
     wrkdetails_name1a:{
       fontSize:10 ,color:'#ff7200', marginTop:25
     },
     wrkdetails_name1b:{
       fontSize:10 ,color:'#69696d', marginTop:10
     },
     wrkdetails_name1c:{
       fontSize:10 ,color:'#69696d', marginTop:10
     },
     wrkdetails_name1abc_mas:{
       position:'absolute',
       right:24,

     },

     save_view : {

       position: 'absolute',
       zIndex:999,
       width:Dimensions.get('window').width,
       justifyContent: 'center',
       backgroundColor: 'transparent'
     },

     signup_btnTxt : {
       fontSize:20,
       paddingTop:35,
       fontWeight:'bold',
       textAlign:'center',
       color:'#fff',
       justifyContent: 'center',
       alignItems: 'center'},

       signup_btnImg:{

           width:220,
           height:100,

            zIndex:999
       },

       signup_view : {

         position: 'absolute',
         zIndex:999,
         width:Dimensions.get('window').width,
         justifyContent: 'center',
         backgroundColor: 'transparent'
       },
       chevron_left_icon : {position:'absolute',paddingLeft:16,paddingTop:16,fontSize:20, zIndex:999},
      linearGradient: {
         flex: 1,
         paddingLeft: 15,
         paddingRight: 15,
         borderRadius: 5
       },
       //workout.js
       text_workout_heading : {fontWeight:'bold',color:'#fff',fontSize:22,marginLeft:20,paddingTop:13,fontFamily:'arial'},
       text_workout_Detailsheading : {fontWeight:'bold',color:'#fff',fontSize:22,marginLeft:20,paddingTop:13,fontFamily:'arial', position:'relative', bottom:0},
       text_workout_sub_heading : {fontSize:10,marginLeft:20,paddingTop:10,color:'#FF7E00'},
       footer_main_view : {flex:1,flexDirection:'row',alignItems:'center',justifyContent:'center',backgroundColor:'#fff',height:70,width:Dimensions.get('window').width,bottom:0},
       icon_style : {flex:0.2,alignItems:'center',justifyContent:'center',paddingBottom:12},
       carousel_style_workout : {width: 270,height: slideHeight,paddingHorizontal: itemHorizontalMargin,paddingBottom: 18},
       carousel_style_trainers : {width: 170,height: slideHeight,paddingHorizontal: itemHorizontalMargin,paddingBottom: 18},
       carousel_style_plans : {width: 270,height: slideHeight,paddingHorizontal: itemHorizontalMargin,paddingBottom: 18},
       carousel_image_view : {flex: 1,backgroundColor: '#2d2e37'},
       carousel_image : {flex: 1,backgroundColor: '#2d2e37',resizeMode:'stretch'},
       carousel_text_view : { paddingTop: 10,paddingBottom: 20,backgroundColor: '#2d2e37'},
       carousel_text : {marginTop: 6,color: '#fff',fontSize: 12,fontStyle: 'italic'},

       //upcomingWorkouts.js
       upcomingWorkouts_main_body : {
                                        backgroundColor: '#2d2e37',
                                        marginTop : 0,
                                        padding:0,
                                        height: Dimensions.get('window').height,
                                        width: Dimensions.get('window').width
                                    },

        upcomingWorkouts_bg_img_view : {
                                            position: 'absolute',
                                            top: 0,
                                            left: 0,
                                            width: undefined,
                                            height: undefined,
                                        },

        upcomingWorkouts_bg_img : {
                                      resizeMode: 'stretch',
                                      height: Dimensions.get('window').height,
                                      width: Dimensions.get('window').width,
                                  },
                                  heart_img_mas:{
                                    position:'relative'
                                  },
                                  heart_img:{
                                    width:96,
                                    height:89,
                                    marginLeft:'6%',
                                     marginTop:60,

                                  },

                                  heart_heading:{
                                    color:'#ffffff',
                                    textAlign:'left', marginTop:5, textAlign:'center', paddingBottom:20,
                                    fontSize : 134,
                                    fontWeight:'bold',paddingTop:15, position:'absolute', top:5, right:20
                                  },



        upcomingWorkouts_heading :{color:'#ffffff',
        textAlign:'left', marginTop:5, marginLeft:'13%',
        fontSize : 20,

        fontWeight:'bold',paddingTop:15},
        basketBall:{
          color:'#ffffff',
          textAlign:'left', marginTop:15, marginLeft:'13%',
          fontSize : 24,
        },
        basketDay:{
          color:'#ffffff',
          textAlign:'left', marginTop:0, marginLeft:'13%',
          fontSize : 12,
        },
        graph_bg:{
          backgroundColor:'#414249',
          width:'93%',
          marginLeft:'3.5%'
        },

        graph_heading:{
          color:'#ffffff',
          textAlign:'left', marginTop:5, textAlign:'center', paddingBottom:20,
          fontSize : 20,
          fontWeight:'bold',paddingTop:15

        },
        gradi_btns:{
          marginTop : 10, marginRight:10,
          alignItems:'center', position:'relative'
        },
        gradi_btnsTxtBtn:{
          position:'absolute', zIndex:999, marginTop:6, paddingRight:8, fontSize:11, fontWeight:'bold', color:'#fff', textAlign:'center'
        },
        course_title1:{
          fontSize:20,
          color:'#fff',
          marginLeft:20,paddingTop:20
        },
        feedrgt_mas:{
          top:25,
          position:'absolute',
          right:20
        },
        feedrgt1:{
          fontSize:12,
          color:'#bcbcbd'
        },
        feedrgt2:{
          fontSize:10,
          color:'#69696d',
          textAlign:'right',
          paddingTop:2
        },

        feed_title1:{
          fontSize:20,
          color:'#fff',
          marginLeft:10,paddingTop:22
        },

        gradi_NormalbtnsTxtBtn:{
          position:'absolute',alignItems:'center', zIndex:999, marginTop:4, paddingRight:2, fontSize:11, fontWeight:'bold', color:'#fff', textAlign:'center'
        },
        slidertop_btns:{
          marginLeft:20, marginTop:5
        },
        listofWrkouts1:{
          backgroundColor:'#242323', paddingBottom:20
        },
        listofWrkouts21:{
          backgroundColor:'#2e2f35', paddingBottom:0
        },
        listofWrkoutsbtm:{
          backgroundColor:'#2e2f35'
        },
        finished_heading:{
          color:'#ffffff',
          textAlign:'center', marginTop:'25%',
          fontSize : 20,
          fontWeight:'bold',paddingTop:15
        },
        wellness:{
          color:'#ffffff',
          textAlign:'center', marginTop:30,
          fontSize : 16,
        },
        being_consistent:{
          width:'60%', marginLeft:'18%', textAlign:'center', marginTop:30,color:'#ffffff'
        },
        keepitup:{
          color:'#ffffff',
          textAlign:'center', marginTop:100,
          fontSize : 16,
        },
        keepitup2:{
          color:'#ffffff',
          textAlign:'center', marginTop:10,
          fontSize : 12, width:'60%', marginLeft:'18%',
        },
        nothanks:{
          color:'#ffffff',
          textAlign:'center', marginTop:30,
          fontSize : 16,
        },
        saveBtn:{
          marginTop : 40,
          alignItems:'center', position:'relative'
        },
        saveBtn2:{
          marginTop : 90,
          alignItems:'center', position:'relative'
        },
        grdBtn2:{
          marginTop : 20,
          alignItems:'center', position:'relative'
        },
        saveTxtBtn:{
          position:'absolute', zIndex:999, marginTop:10, fontSize:18, fontWeight:'bold', color:'#e25269'
        },
        gradiPagBtns:{
          height:28,
          width:86,
          borderColor:'#504f4f',
          borderRadius:30,
          borderWidth:1,position:'relative', top:-3
        },

        upcomingWorkouts_scrollView : {
          marginTop : 5,
          marginBottom:170

        },
        allMiddleIcon:{
          position:'absolute',
          top:10,zIndex:999
        },

        upcomingWorkouts_content_view : {
            alignItems:'center'
        },
btm_tabNavigation:{
  position:'absolute',
  bottom:0,
  backgroundColor:'#414249',flex:1,flexDirection:'row',paddingTop:12,paddingBottom:2
},
trans_bg:{


  flex:1,
  flexDirection:'column',
  alignItems:'center',
  justifyContent:'center',

  position:'absolute',
  bottom:0,
  resizeMode: 'stretch',
  height:87,zIndex:2,
  width:Dimensions.get('window').width,
  backgroundColor: 'rgba(237, 75, 60, 0.8)'
},
cross_icon:{
  position:'absolute',
  bottom:28,
  zIndex:3,
  flex:1,
  flexDirection:'column',
  alignItems:'center',
  justifyContent:'center',
  width:Dimensions.get('window').width


},
        upcomingWorkouts_content : {
          width:'75%'
        },
         whiteline:{
           borderBottomWidth:3,borderBottomColor : '#FFF',marginBottom:7, marginTop:7
         },

        tuesday_heading:{
          fontSize:12,
          color:'#ffffff',marginTop:0
        },
        wat_log_track_icons:{
       flex:1,flexDirection:'row',paddingLeft:20,paddingRight:20,fontSize:15,paddingTop:20,
       position:'absolute', bottom:100
     },
     star_icons:{
       flex:1,flexDirection:'row',paddingTop:40, width:'60%', marginLeft:'20%',
       alignItems: 'center',
       justifyContent:'center'
     },
     feed_img_mas:{
       flex:1,flexDirection:'row',
       marginLeft:20
     },


        upcomingWorkouts_workout_img : {height:220,
        width:300},

        upcomingWorkouts_footer : {
          alignItems:'center',
          justifyContent:'center',
          flexDirection:'row',
          position:'relative',
          bottom:0
        },
        modal:{
          flex: 1,
      alignItems: 'center',
      backgroundColor: '#f7021a',
      padding: 100
    },
    textdd:{
      position:'absolute',
      top:100
    },

        btm_cross:{
          position:'absolute',
          top:450, zIndex:99,
          height:Dimensions.get('window').height,
          elevation:999995, color:'#fff'
        },

        upcomingWorkouts_footer_view : {

          position: 'absolute',
          zIndex:999,
          width:Dimensions.get('window').width,
          justifyContent: 'center',
          backgroundColor: 'transparent'
        },
        Searching1:{
          fontSize:14,
          color:'#828385', borderBottomColor:'#202124', borderBottomWidth:1,
          width:'100%', paddingLeft:20, paddingTop: 15, paddingBottom:15
        },
        connectr_name:{
          fontSize:14,
          color:'#fff', borderBottomColor:'#202124', borderBottomWidth:1,
          width:'100%', paddingLeft:20, paddingTop: 20, paddingBottom:20
        },
        connected_txt1:{
          fontSize:14,
          color:'#828385',
          flex:1,
          flexDirection:'column'
        },
        connected_txt1Mas:{
          position:'absolute',
          right:16,top:18,
          flex:1,
          flexDirection:'row', width:'45%'
        },

        upcomingWorkouts_footer_content: {
          fontSize:20,
          paddingTop:35,
          fontWeight:'bold',
          textAlign:'center',
          color:'#fff',
          justifyContent: 'center',
          alignItems: 'center'},


});


export default styles;
