/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput

} from 'react-native';

import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';
import { LineChart, YAxis } from 'react-native-svg-charts'
import { LinearGradient, Stop } from 'react-native-svg'

import * as shape from 'd3-shape'
const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');

import SplashScreen from 'react-native-smart-splash-screen'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import  Entypo from 'react-native-vector-icons/Entypo'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import Feather from 'react-native-vector-icons/Feather'
import Ionicons from 'react-native-vector-icons/Ionicons'

import { TabNavigator } from 'react-navigation';


import styles from './styles.js'



export default class WorkoutSummary extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
      width:0,
      height : 0
    };
  }
  componentDidMount () {


  }



  render() {
    const {navigation} = this.props;
    const data = [ 50, 10, 40, 95, -4, -24, 85, 91, 35, 53, -53, 24, 50, -20, -80 ]


    return (
      <View style={styles.upcomingWorkouts_main_body}>
              <View style={styles.upcomingWorkouts_bg_img_view}>
                <Image style={styles.upcomingWorkouts_bg_img} source={{ uri: 'gradientbg' }}/>
              </View>
              <View style={styles.upcomingWorkouts_header}>
              <Text style={styles.graph_heading}>
                  TRACK ACTIVITY
              </Text>
              </View>
              <View style={styles.heart_img_mas}>
                <Image style={styles.heart_img} source={{ uri: 'heart' }}/>
                <Text style={styles.heart_heading}>
                    183
                </Text>
              </View>




              <View style={styles.cross_icon}>
                <Image style={{  width: 45, height: 45 }} source={{uri:'cross'}} />

              </View>
              <View style={styles.trans_bg}>

              </View>
              <View style={styles.btm_tabNavigation}>
                  <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 32, height: 28, top:3, position:'absolute' }} source={{uri:'feedicon'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}>FEED</Text></View>
                  <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 28, height: 28, top:3, position:'absolute'  }} source={{uri:'profileicon'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}>PROFILE</Text></View>
                  <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 45, height: 45, marginTop:3 }} source={{uri:'bottom_tab_middlelogo'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}> </Text></View>
                  <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 28, height: 31, top:3, position:'absolute'  }} source={{uri:'notificationicon'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}>NOTIFICATION</Text></View>
                  <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 28, height: 28, top:3, position:'absolute'  }} source={{uri:'btnmenuicon'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}>MORE</Text></View>


              </View>


            </View>



    );
  }
}
