/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, { Component } from 'react';
import {
Platform,
StyleSheet,
Text,
View,
Switch,
Image,
PixelRatio,
KeyboardAvoidingView,
ScrollView,
TouchableHighlight,
TouchableOpacity
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Carousel, { Pagination,ParallaxImage } from 'react-native-snap-carousel';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { NavigationActions } from 'react-navigation';
const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import  Entypo from 'react-native-vector-icons/Entypo'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import Feather from 'react-native-vector-icons/Feather'

import SplashScreen from 'react-native-smart-splash-screen'

import styles from './styles.js'
import { ENTRIES1 } from './entries';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function wp (percentage) {
  const value = (percentage * viewportWidth) / 100;
  return Math.round(value);
}

const slideHeight = viewportHeight * 0.4;
const slideWidth = wp(90);
const itemHorizontalMargin = wp(2);
const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;


export default class Feed extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
      width:0,
      height : 0,
      slider1Ref :null,
      slider1ActiveSlide: 1,
    };
  }

  _renderWorkout= ({item, index},parallaxProps)=> {

    return (
        <View >
          <TouchableOpacity
              activeOpacity={1}
              style={styles.carousel_style_workout}
              onPress={() => { alert('hi'); }}
              >
                <View style={styles.carousel_image_view}>

                    <ParallaxImage
                    source={{ uri: item.illustration }}
                    containerStyle={{flex: 1,backgroundColor: 'white',}}
                    style={styles.carousel_image}
                    parallaxFactor={0.35}
                    showSpinner={true}
                    spinnerColor={'rgba(0, 0, 0, 0.25)'}
                    {...parallaxProps}
            />

                </View>
                <View style={styles.carousel_text_view}>
                    <Text
                      style={styles.carousel_text}
                      numberOfLines={2}
                    >
                      some text
                    </Text>

                </View>
            </TouchableOpacity>
        </View>
    );
}


    _renderTrainers= ({item, index},parallaxProps)=> {

      return (
          <View >
            <TouchableOpacity
                activeOpacity={1}
                style={styles.carousel_style_trainers}
                onPress={() => { alert('hi'); }}
                >
                  <View style={styles.carousel_image_view}>

                      <ParallaxImage
                      source={{ uri: item.illustration }}
                      containerStyle={{flex: 1,backgroundColor: 'white',}}
                      style={styles.carousel_image}
                      parallaxFactor={0.35}
                      showSpinner={true}
                      spinnerColor={'rgba(0, 0, 0, 0.25)'}
                      {...parallaxProps}
              />

                  </View>
                  <View style={styles.carousel_text_view}>
                      <Text
                        style={styles.carousel_text}
                        numberOfLines={2}
                      >
                      some text
                      </Text>

                  </View>
              </TouchableOpacity>
          </View>
      );
  }


      _renderPlans= ({item, index},parallaxProps)=> {

        return (
            <View >
              <TouchableOpacity
                  activeOpacity={1}
                  style={styles.carousel_style_plans}
                  onPress={() => { alert('hi'); }}
                  >
                    <View style={styles.carousel_image_view}>

                        <ParallaxImage
                        source={{ uri: item.illustration }}
                        containerStyle={{flex: 1,backgroundColor: 'white',}}
                        style={styles.carousel_image}
                        parallaxFactor={0.35}
                        showSpinner={true}
                        spinnerColor={'rgba(0, 0, 0, 0.25)'}
                        {...parallaxProps}
                />

                    </View>
                    <View style={styles.carousel_text_view}>
                        <Text
                          style={styles.carousel_text}
                          numberOfLines={2}
                        >
                          some text
                        </Text>

                    </View>
                </TouchableOpacity>
            </View>
        );
    }


render() {
  const {navigation} = this.props
  const resizeMode = 'center';
  const text = 'I am some centered text';
  return (

    <View style={styles.mainBody} >


          <ScrollView
            style={{flex:1}}
            contentContainerStyle={{paddingBottom: 50}}
            indicatorStyle={'white'}
            scrollEventThrottle={200}
            directionalLockEnabled={true}
          >
            <View style={styles.listofWrkouts1}>
              <View style={styles.chevron_left_icon}>
                <TouchableOpacity onPress={()=>this.onPressChevronLeft(navigation )}>
                    <FontAwesome name="chevron-left" size={25} color="#FF7E00"   />
                </TouchableOpacity>
              </View>

              <View style={styles.header}>
                    <Text style={styles.topSignupTxt}>
                      Feed
                    </Text>
              </View>

              <View>
                <Text style = {styles.text_workout_heading}>
                  Hey Sophie!
                </Text>
                <Text style = {styles.text_workout_sub_heading}>
                  REMAINDER TO STAY STRONG AND KEEP YOUR GOALS IN MIND
                </Text>
              </View>

              <View style={styles.slidertop_btns}>
                <View style={{flex:1,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                  <View style = {{flex:0.28,alignItems:'center',justifyContent:'center',}}>
                  <TouchableOpacity>
                    <View style={styles.gradi_btns}>
                      <Text style={styles.gradi_NormalbtnsTxtBtn}>
                          Distance
                      </Text>
                      <View style={styles.gradiPagBtns}></View>
                    </View>
                  </TouchableOpacity>
                  </View>
                  <View style = {{flex:0.28,alignItems:'center',justifyContent:'center',}}>
                  <TouchableOpacity>
                  <View style={styles.gradi_btns}>
                    <Text style={styles.gradi_NormalbtnsTxtBtn}>
                        Weights
                    </Text>
                    <View style={styles.gradiPagBtns}></View>
                  </View>
                  </TouchableOpacity>
                  </View>
                  <View style = {{flex:0.28,alignItems:'center',justifyContent:'center',}}>
                  <TouchableOpacity>
                  <View style={styles.gradi_btns}>
                    <Text style={styles.gradi_btnsTxtBtn}>
                        Conditioning
                    </Text>
                    <Image style={{ width: 86, height: 32 }} source={{uri:'gradi_btn'}} />
                    </View>
                  </TouchableOpacity>
                  </View>
                  <View style = {{flex:0.16,alignItems:'center',justifyContent:'center',}}>
                    <TouchableOpacity >
                        <View>
                          <Feather name="sliders" size={25} color='#fff' />
                        </View>
                    </TouchableOpacity>




                  </View>

                </View>
              </View>
            </View>

            <View style={styles.feed_img_mas}>
              <Image style={{ width: 45, height: 45, marginTop:15 }} source={{uri:'feed_img'}} />
              <Text style={styles.feed_title1}>Lindsey Baah</Text>
              <View style={styles.feedrgt_mas}>
                <Text style={styles.feedrgt1}>Cycling workout</Text>
                <Text style={styles.feedrgt2}>53 mins ago</Text>
              </View>
            </View>
            <View>

                  <Carousel
                    ref={(c) => { if (!this.state.slider1Ref) { this.setState({ slider1Ref: c }); } }}
                    data={ENTRIES1}
                    renderItem={this._renderWorkout}
                    sliderWidth={sliderWidth}
                    itemWidth={200}
                    itemHeight={300}
                    hasParallaxImages={true}
                    firstItem={1}
                    inactiveSlideScale={0.94}
                    inactiveSlideOpacity={0.7}
                    enableMomentum={false}
                    containerCustomStyle={{marginTop: 15}}
                    contentContainerCustomStyle={{}}
                    loop={true}
                    onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index }) }
                  />
              </View>


          </ScrollView>




          <View style={styles.btm_tabNavigation}>
              <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 32, height: 28, top:3, position:'absolute' }} source={{uri:'feedicon'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}>FEED</Text></View>
              <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 28, height: 28, top:3, position:'absolute'  }} source={{uri:'profileicon'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}>PROFILE</Text></View>
              <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 45, height: 45, marginTop:3 }} source={{uri:'bottom_tab_middlelogo'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}> </Text></View>
              <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 28, height: 31, top:3, position:'absolute'  }} source={{uri:'notificationicon'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}>NOTIFICATION</Text></View>
              <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 28, height: 28, top:3, position:'absolute'  }} source={{uri:'btnmenuicon'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}>MORE</Text></View>
          </View>



          </View>

  );
}
}
